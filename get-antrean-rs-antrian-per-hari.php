<?php
include 'signature.php';

// Database connection
$serverName = "10.50.151.154"; // sesuaikan dengan nama server Anda
$username = "op"; // sesuaikan dengan username Anda
$password = "op"; // sesuaikan dengan password Anda
$database = "Ciracas"; // sesuaikan dengan nama database Anda

// Establishes the connection
$connectionInfo = array(
    "Database" => $database,
    "UID" => $username,
    "PWD" => $password
);
$conn = sqlsrv_connect($serverName, $connectionInfo);

if ($conn === false) {
    die(print_r(sqlsrv_errors(), true));
}

$inputEndPoint = "/antrean/pendaftaran/tanggal/" . date("Y-m-d");

$key = "$consId$secretKey$timeStamp";

// full url() bpjs
function url($baseUrl, $serviceName, $endPoint)
{
    return $baseUrl . $serviceName . $endPoint;
}

function encryptSignature($consId, $secretKey, $timeStamp)
{
    $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
    $encodedSignature = base64_encode($signature);

    return $encodedSignature;
}

function decryptSignature($key, $string)
{
    $encrypt_method = 'AES-256-CBC';

    // hash
    $key_hash = hex2bin(hash('sha256', $key));

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

    return $output;
}

// download libraries lzstring : https://github.com/nullpunkt/lz-string-php
function decompress($string)
{
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZString.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZContext.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZData.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil16.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZReverseDictionary.php";
    return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
}

// Function to retrieve patient details from database
function getPatientDetails($conn, $noCM)
{
    $sql = "SELECT * FROM Pasien WHERE NoCM = ?";
    $params = array($noCM);
    $stmt = sqlsrv_query($conn, $sql, $params);

    if ($stmt === false) {
        die(print_r(sqlsrv_errors(), true));
    }

    $patientDetails = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    sqlsrv_free_stmt($stmt);

    return $patientDetails;
}

try {
    $headers = array(
        "Content-Type: application/json",
        "Accept: application/json",
        "x-cons-id: $consId",
        "x-timestamp: $timeStamp",
        "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
        "user_key: $userKey"
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    $result = json_decode($response, true);
    $decrypt = decryptSignature($consId . $secretKey . $timeStamp, $result['response']);
    $jsonData = decompress($decrypt);
    $objects = json_decode($jsonData);

    // Format tampilan dengan setiap objek pada baris terpisah
    $formattedData = $objects;

    // Ambil data pasien dari database dan gabungkan dengan $formattedData
    foreach ($formattedData as $index => $item) {
        $patientDetails = getPatientDetails($conn, $item->norekammedis);
        if (!empty($patientDetails)) {
            // Tambahkan data pasien ke dalam objek $item
            $item->NamaLengkap = $patientDetails['NamaLengkap'];
            $item->JenisKelamin = $patientDetails['JenisKelamin'];
            $item->TglLahir = !empty($patientDetails['TglLahir']) ? date_format($patientDetails['TglLahir'], 'd-m-Y') : null;
            $item->Alamat = $patientDetails['Alamat'];
            // Tambahkan kolom lain yang diperlukan dari tabel Pasien
        }
    }

    // Hitung total pasien berdasarkan status
    $totalPasien = count($formattedData);
    $totalSelesai = 0;
    $totalSedang = 0;
    $totalBelum = 0;

    foreach ($formattedData as $item) {
        if ($item->status == 'Selesai dilayani') {
            $totalSelesai++;
        } elseif ($item->status == 'Sedang dilayani') {
            $totalSedang++;
        } elseif ($item->status == 'Belum dilayani') {
            $totalBelum++;
        }
    }

} catch (\Throwable $th) {
    echo '<p style="color: red;">An error occurred: ' . $th->getMessage() . '</p>';
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Data JSON</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
            text-align: left;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ccc;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:nth-child(even) {
            background-color: #f9f9f9;
        }
        .completed {
            background-color: #d4edda;
        }
        .in-progress {
            background-color: #fff3cd;
        }
        .pending {
            background-color: #f8d7da;
        }
        button {
            background-color: #4CAF50;
            color: white;
            border: none;
            padding: 10px;
            cursor: pointer;
        }
        button:hover {
            background-color: #45a049;
        }
        .container {
            max-width: 1200px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Data Bridging JKN Tanggal <?php echo date('d-m-Y'); ?></h1>

    <!-- Informasi Total Pasien -->
    <p>Total Pasien: <?php echo $totalPasien; ?></p>
    <p>Total Pasien Selesai dilayani: <?php echo $totalSelesai; ?></p>
    <p>Total Pasien Sedang dilayani: <?php echo $totalSedang; ?></p>
    <p>Total Pasien Belum dilayani: <?php echo $totalBelum; ?></p>

    <table>
        <thead>
        <tr>
            <th>No</th>
            <th>Nama Pasien</th>
            <th>Jenis Kunjungan</th>
            <th>Nomor Referensi</th>
            <th>Created Time</th>
            <th>Kode Booking</th>
            <th>No. Rekam Medis</th>
            <th>NIK</th>
            <th>No. KAPST</th>
            <th>No. Antrean</th>
            <th>Kode Poli</th>
            <th>Sumber Data</th>
            <th>Estimasi Dilayani</th>
            <th>Kode Dokter</th>
            <th>Jam Praktek</th>
            <th>No. HP</th>
            <th>Tanggal</th>
            <th>Is Peserta</th>
            <th>Status</th>
            <th>Status Detail</th>
            <!-- Tambahkan kolom lain yang diperlukan dari tabel Pasien -->
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($formattedData)): ?>
            <?php foreach ($formattedData as $index => $item): ?>
                <tr style="background-color: <?php echo htmlspecialchars($item->status) == 'Selesai dilayani' ? '#d4edda' : (htmlspecialchars($item->status) == 'Sedang dilayani' ? '#fff3cd' : '#f8d7da'); ?>">
                    <td><?php echo $index + 1; ?></td>
                    <td><?php echo !empty($item->NamaLengkap) ? htmlspecialchars($item->NamaLengkap) : 'Nama tidak tersedia'; ?></td>
                    <td><?php echo htmlspecialchars($item->jeniskunjungan); ?></td>
                    <td><?php echo htmlspecialchars($item->nomorreferensi); ?></td>
                    <td><?php echo htmlspecialchars($item->createdtime); ?></td>
                    <td><?php echo htmlspecialchars($item->kodebooking); ?></td>
                    <td><?php echo htmlspecialchars($item->norekammedis); ?></td>
                    <td><?php echo htmlspecialchars($item->nik); ?></td>
                    <td><?php echo htmlspecialchars($item->nokapst); ?></td>
                    <td><?php echo htmlspecialchars($item->noantrean); ?></td>
                    <td><?php echo htmlspecialchars($item->kodepoli); ?></td>
                    <td><?php echo htmlspecialchars($item->sumberdata); ?></td>
                    <td><?php echo htmlspecialchars($item->estimasidilayani); ?></td>
                    <td><?php echo htmlspecialchars($item->kodedokter); ?></td>
                    <td><?php echo htmlspecialchars($item->jampraktek); ?></td>
                    <td><?php echo htmlspecialchars($item->nohp); ?></td>
                    <td><?php echo htmlspecialchars($item->tanggal); ?></td>
                    <td><?php echo htmlspecialchars($item->ispeserta ? 'Yes' : 'No'); ?></td>
                    <td><?php echo htmlspecialchars($item->status); ?></td>
                    <td><button onclick="showDetail('<?php echo htmlspecialchars($item->kodebooking); ?>')">Detail</button></td>
                    
                    <!-- Tambahkan kolom lain yang diperlukan dari tabel Pasien -->
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="22">No data available</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<script>
    function showDetail(kodeBooking) {
        // Kirim permintaan ke script kedua untuk menampilkan data JSON berdasarkan kode booking
        window.location.href = 'http://10.50.151.155/rsudciracas/bridgingbpjs/post-antrean-rs-5-gettasklist.php?kodeBooking=' + encodeURIComponent(kodeBooking);
    }
</script>
</body>
</html>
