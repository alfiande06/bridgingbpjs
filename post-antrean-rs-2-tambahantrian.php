    <?php

    $NoBooking = $_POST['KodeBooking'];
    // $KdRuangan = $_POST['KdRuangan'];
    // $KdSubInstalasi = $_POST['KdSubInstalasi'];

    include("sqlsrv.php");
    include 'signature.php';

    $timeStamp2 = (time() - strtotime('1970-01-01 00:00:00')) * 1000;

    // input dari simrs
    $inputEndPoint = "/antrean/add"; //$_GET['end_point'];
    $tanggalmasuk = date('Y-m-d', strtotime($_POST['TglMasuk']));

    $inputPayload = array(
        "kodebooking" => $NoBooking, // Mengganti nilai kodebooking dengan data dari POST
        "jenispasien" => $_POST['JenisPasien'], // Misalnya, mengambil JenisPasien dari POST
        "nomorkartu" => $_POST['NoKartu'], // Mengambil NoKartu dari POST
        "nik" => $_POST['NIK'], // Mengambil NIK dari POST
        "nohp" => $_POST['nohp'], // Mengambil nohp dari POST
        "kodepoli" => $_POST['kodepoli'], // Mengambil kodepoli dari POST
        "namapoli" => $_POST['namapoli'], // Mengambil namapoli dar  i POST
        "pasienbaru" => $_POST['PasienBaru'], // Mengambil PasienBaru dari POST
        "norm" => $_POST['NoCM'], // Mengambil NoCM dari POST
        "tanggalperiksa" => $tanggalmasuk, // Mengambil TglMasuk dari POST
        "kodedokter" => $_POST['kodedokter'], // Mengambil kodedokter dari POST
        "namadokter" => $_POST['NamaDokter'], // Mengambil NamaDokter dari POST
        "jampraktek" => $_POST['JamPraktek'], // Mengambil JamPraktek dari POST
        "jeniskunjungan" => $_POST['JenisKunjungan'], // Mengambil JenisKunjungan dari POST
        "nomorreferensi" => $_POST['NomorRefrensi'], // Mengambil NomorRefrensi dari POST
        "nomorantrean" => $_POST['noantrean'], // Mengambil noantrean dari POST
        "angkaantrean" => $_POST['angkaantrean'], // Mengambil angkaantrean dari POST
        "estimasidilayani" => $timeStamp2, // Menggunakan timestamp yang telah Anda definisikan sebelumnya
        "sisakuotajkn" => $_POST['sisakuotajkn'], // Mengambil sisakuotajkn dari POST
        "kuotajkn" => $_POST['kuotajkn'], // Mengambil kuotajkn dari POST
        "sisakuotanonjkn" => $_POST['sisakuotanonjkn'], // Mengambil sisakuotanonjkn dari POST
        "kuotanonjkn" => $_POST['kuotanonjkn'], // Mengambil kuotanonjkn dari POST
        "keterangan" => $_POST['Keterangan'] // Mengambil Keterangan dari POST
    );
 
    // full url() bpjs
    function url($baseUrl, $serviceName, $endPoint)
    {
        return $baseUrl . $serviceName . $endPoint;
    }

    function encryptSignature($consId, $secretKey, $timeStamp)
    {
        $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);

        return $encodedSignature;
    }

    // payload for mapping data simrs
    function payload($payload)
    {
        return json_encode($payload);
    }

    function decryptSignature($key, $string)
    {
        $encrypt_method = 'AES-256-CBC';

        // hash
        $key_hash = hex2bin(hash('sha256', $key));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

        return $output;
    }

    // download libraries lzstring : https://github.com/nullpunkt/lz-string-php
    function decompress($string)
    {
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }

    try {
        $headers = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "x-cons-id: $consId",
            "x-timestamp: $timeStamp",
            "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
            "user_key: $userKey"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, payload($inputPayload));
        $response = curl_exec($ch);
        // echo payload($inputPayload);
        $payloadtest = payload($inputPayload);
        echo "<script>alert('$payloadtest');console.log('$payloadtest');</script>";
        echo "<script>alert('$response'); console.log('$payloadtest'); window.location.href='../siregist/page.php?modul=pendaftaran';</script>";
    } catch (\Throwable $th) {
        echo $th->getMessage();
        echo "<script>alert('Anda dialihkan ke situs iCare');  window.location.href='../siregist/page.php?modul=pendaftaran';</script>";
    }

    ?>