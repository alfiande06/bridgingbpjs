<?php
require 'vendor/autoload.php'; // Memasukkan pustaka Firebase JWT

use \Firebase\JWT\JWT;
// require 'JWT.php';
$headers = apache_request_headers();
$username = $headers['x-username'];
$password = $headers['x-password'];

function generateToken($username, $password) {
    if ($username == 'bpjs' && $password == 'bpjstku') {
        $secretKey = 'bpjskesehatan'; // Ganti dengan kunci rahasia yang kuat
        $currentTime = time();
        $expirationTime = $currentTime + 86400; // 1 jam (3600 detik) dari sekarang
        $payload = [
            'username' => $username,
            'password' => $password,
            'exp' => $expirationTime
        ];

        // Membuat token JWT
        $token = JWT::encode($payload, $secretKey, 'HS256');
        // $token = 'tes123';

        return $token;
    } else {
        return null;
    }
}

$token = generateToken($username, $password);

if ($token !== null) {
    $hasil = [
        "response" => [
            "token" => $token
        ],
        "metadata" => [
            "message" => "ok",
            "code" => 200
        ]
    ];
} else {
    $hasil = [
        "metadata" => [
            "message" => "error",
            "code" => 404
        ]
    ];
}

header('Content-Type: application/json');
echo json_encode($hasil);
?>
