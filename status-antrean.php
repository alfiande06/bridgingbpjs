<?php
include("sqlsrv.php");
$headers = apache_request_headers();

$json = file_get_contents('php://input');
$data = json_decode($json, true);
$jampraktek = $data['jampraktek'];
$username = $headers['x-username'];
$password = $headers['x-password'] ?? '';
$token = $headers['x-token'];

require 'vendor/autoload.php'; // Memasukkan pustaka Firebase JWT
use \Firebase\JWT\JWT;

$secretKey = 'bpjskesehatan';

try {
    // Mendekode token JWT
    $decoded = JWT::decode($token, $secretKey, ['HS256']);
    
    // Periksa apakah username dan password sesuai
    if ($decoded->username !== $username && $decoded->password !== $password) {
        $hasil = [
            "response" => null,
            "metadata" => [
                "message" => "Username or password is incorrect",
                "code" => 401 // Unauthorized
            ]
        ];
    } else {
        // Token valid, lanjutkan seperti sebelumnya
        $sql = "SELECT * from MasterJadwalPraktekDokter a 
            join DataPegawai b on a.IdDokter = b.IdPegawai
            join Ruangan c on a.KdRuangan = c.KdRuangan where idpegawai = 'P000000147'";
        $sql2 = sqlsrv_query($conn, $sql);
        
        while ($row = sqlsrv_fetch_array($sql2, SQLSRV_FETCH_ASSOC)) {
            // Tambahkan data hasil ke dalam $response
            $response[] = [
                "namapoli" => "Anak",
                "namadokter" => "Dr. Hendra",
                "totalantrean" => 25,
                "sisaantrean" => 4,
                "antreanpanggil" => $row['NamaLengkap'],
                "sisakuotajkn" => 5,
                "kuotajkn" => 30,
                "sisakuotanonjkn" => 5,
                "kuotanonjkn" => 30,
                "keterangan" => $decoded->username
            ];
        }
        
        $hasil = [
            "response" => $response,
            "metadata" => [
                "message" => "Ok",
                "code" => 200
            ]
        ];
    }
//jika token kadaluarsa akan secara automatis masuk ke catch
} catch (Exception $e) {
    // Token telah kadaluwarsa
    $hasil = [
        "response" => null,
        "metadata" => [
            "message" => "Token expired or invalid",
            "code" => 401 // Unauthorized
        ]
    ];
}

header('Content-Type: application/json');
echo json_encode($hasil);
?>
