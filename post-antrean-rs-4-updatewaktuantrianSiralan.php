    <?php

    include 'signature.php';

    // input dari simrs
    $inputEndPoint = "/antrean/updatewaktu"; //$_GET['end_point'];

    date_default_timezone_set('Asia/Jakarta'); // Mengatur zona waktu ke WIB (Waktu Indonesia Barat)

    $timeStamp2 = (time() - strtotime('1970-01-01 00:00:00')) * 1000;

    $inputPayload = array(
        "kodebooking" => $_POST['codeboking'],
        "taskid" => $_POST['taskid'],
        "waktu" => $timeStamp2,
        "jenisresep" => ""
    );
 
    // full url() bpjs
    function url($baseUrl, $serviceName, $endPoint)
    {
        return $baseUrl . $serviceName . $endPoint;
    }

    function encryptSignature($consId, $secretKey, $timeStamp)
    {
        $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);

        return $encodedSignature;
    }

    // payload for mapping data simrs
    function payload($payload)
    {
        return json_encode($payload);
    }

    function decryptSignature($key, $string)
    {
        $encrypt_method = 'AES-256-CBC';

        // hash
        $key_hash = hex2bin(hash('sha256', $key));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

        return $output;
    }

    // download libraries lzstring : https://github.com/nullpunkt/lz-string-php
    function decompress($string)
    {
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }

    try {
        $headers = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "x-cons-id: $consId",
            "x-timestamp: $timeStamp",
            "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
            "user_key: $userKey"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, payload($inputPayload));
        $response = curl_exec($ch);
        echo "<script>alert('$response'); location.reload();</script>";
    } catch (\Throwable $th) {
        echo $th->getMessage();
        echo "<script>alert('$response'); location.reload();</script>";
    }

    ?>