<?php
    // require __DIR__ . '/vendor/autoload.php';

    // use LZCompressor\LZString;

    // include 'signature.php';
    $baseUrl = "https://apijkn-dev.bpjs-kesehatan.go.id";
    // $baseUrl = "https://apijkn-dev.bpjs-kesehatan.go.id";
    // $serviceName = "/vclaim-rest-dev";
    $serviceName = "/antreanrs_dev";
    $consId = "5096";
    $secretKey = "7rX4315B0C";
    date_default_timezone_set('UTC');
    $timeStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
    $userKey = "f6eb784e640297ea0c8fd9923e879d87";
    // input dari simrs
    $inputEndPoint = "/dashboard/waktutunggu/bulan/09/tahun/2023/waktu/server"; //$_GET['end_point'];
    
    $key = "$consId$secretKey$timeStamp";

    // full url() bpjs
    function url($baseUrl, $serviceName, $endPoint)
    {
        return $baseUrl . $serviceName . $endPoint;
    }

    function encryptSignature($consId, $secretKey, $timeStamp)
    {
        $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);

        return $encodedSignature;
    }

    function decryptSignature($key, $string)
    {
        $encrypt_method = 'AES-256-CBC';

        // hash
        $key_hash = hex2bin(hash('sha256', $key));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

        return $output;
    }

    // download libraries lzstring : https://github.com/nullpunkt/lz-string-php
    function decompress($string)
    {
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZString.php";
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZContext.php";
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZData.php";
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil.php";
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil16.php";
        require_once "vendor/lz-string-php-master/src/LZCompressor/LZReverseDictionary.php";
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }

    try {
        $headers = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "x-cons-id: $consId",
            "x-timestamp: $timeStamp",
            "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
            "user_key: $userKey"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        $result = json_decode($response, true);
        echo decompress($consId . $secretKey . $timeStamp);
        // echo $result['response'];
        $jsonString = json_encode($result['response']);
        // echo $jsonString;
        // var_dump($result['response']);
        if (is_string($jsonString)) {
            // $decrypt = decryptSignature($consId . $secretKey . $timeStamp, $result['response']);
            // $jsonData = decompress($decrypt);
            $objects = json_decode($jsonString);
    
            $formattedData = '';
            foreach ($objects as $object) {
                $formattedData .= json_encode($object, JSON_PRETTY_PRINT) . "\n";
            }
        } else {
            $formattedData = 'Response is not a valid string.';
        }
        // curl_close($ch);
    } catch (\Throwable $th) {
        echo '<p style="color: red;">An error occurred: ' . $th->getMessage() . '</p>';
    }
    ?>

<!DOCTYPE html>
<html>
<head>
    <title>Data JSON</title>
    <style>
        pre {
            background-color: #f4f4f4;
            padding: 10px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
            white-space: pre-wrap;
        }
    </style>
</head>
<body>
    <h1>Data JSON</h1>
    <pre><?php echo $formattedData; ?></pre>
</body>
</html>