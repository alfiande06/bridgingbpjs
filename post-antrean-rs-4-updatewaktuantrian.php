    <?php
include 'signature.php';

    



    // input dari simrs
    $inputEndPoint = "/antrean/updatewaktu"; //$_GET['end_point'];

    date_default_timezone_set('Asia/Jakarta'); // Mengatur zona waktu ke WIB (Waktu Indonesia Barat)


 
    // full url() bpjs
    function url($baseUrl, $serviceName, $endPoint)
    {
        return $baseUrl . $serviceName . $endPoint;
    }

    function encryptSignature($consId, $secretKey, $timeStamp)
    {
        $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);

        return $encodedSignature;
    }

    // payload for mapping data simrs
    function payload($payload)
    {
        return json_encode($payload);
    }

    function decryptSignature($key, $string)
    {
        $encrypt_method = 'AES-256-CBC';

        // hash
        $key_hash = hex2bin(hash('sha256', $key));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

        return $output;
    }

    // download libraries lzstring : https://github.com/nullpunkt/lz-string-php
    function decompress($string)
    {
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }
    for ($i = 1; $i <= 5; $i++) {
    try {

        $headers = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "x-cons-id: $consId",
            "x-timestamp: $timeStamp",
            "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
            "user_key: $userKey"
        );
            date_default_timezone_set('Asia/Jakarta');
            static $previousTime = 0; // Variabel untuk menyimpan waktu sebelumnya

            if ($previousTime === 0) {
                // Iterasi pertama: gunakan waktu saat ini
                $previousTime = (time() - strtotime('1970-01-01 00:00:00'));
            } else {
                // Iterasi berikutnya: tambahkan random menit (1 sampai 10 menit)
                $previousTime += rand(1, 10) * 60 + rand(0, 59); // Menambah random menit dan detik // Random dalam detik
            }
            
            $timeStamp2 = $previousTime * 1000; // Konversi ke milidetik
            
            
            // tambahkan jeda 15 detik per iterasi

            $inputPayload = array(
                "kodebooking" => $_POST['KodeBooking2'],
                "taskid" => $i,
                "waktu" => $timeStamp2,
                "jenisresep" => ""
            );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, payload($inputPayload));
        $response = curl_exec($ch);
        
        
        
    } catch (\Throwable $th) {
        echo $th->getMessage();
        echo "<script>alert('$response'); console.log($response); window.location.href='../siregist/page.php?modul=pendaftaran';</script>";
    }
    $timeStamp2 += 5000;
}   echo "<script>alert('$response'); console.log($response); window.location.href='../siregist/page.php?modul=pendaftaran';</script>";
    
    ?>