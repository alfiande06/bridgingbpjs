<?php
include 'signature.php';

// input dari simrs
$inputEndPoint = "/antrean/getlisttask"; //$_GET['end_point']

$inputPayload = array(
    "kodebooking" => $_GET['kodeBooking']
);

// full url() bpjs
function url($baseUrl, $serviceName, $endPoint)
{
    return $baseUrl . $serviceName . $endPoint;
}

function encryptSignature($consId, $secretKey, $timeStamp)
{
    $signature = hash_hmac('sha256', $consId . "&" . $timeStamp, $secretKey, true);
    $encodedSignature = base64_encode($signature);

    return $encodedSignature;
}

// payload for mapping data simrs
function payload($payload)
{
    return json_encode($payload);
}

function decryptSignature($key, $string)
{
    $encrypt_method = 'AES-256-CBC';

    // hash
    $key_hash = hex2bin(hash('sha256', $key));

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

    return $output;
}

// download libraries lzstring : https://github.com/nullpunkt/lz-string-php
function decompress($string)
{
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZString.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZContext.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZData.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZUtil16.php";
    require_once "vendor/lz-string-php-master/src/LZCompressor/LZReverseDictionary.php";
    return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
}

try {
    $headers = array(
        "Content-Type: application/json",
        "Accept: application/json",
        "x-cons-id: $consId",
        "x-timestamp: $timeStamp",
        "x-signature: " . encryptSignature($consId, $secretKey, $timeStamp),
        "user_key: $userKey"
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url($baseUrl, $serviceName, $inputEndPoint));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, payload($inputPayload));
    $response = curl_exec($ch);
    $result = json_decode($response, true);
    $decrypt = decryptSignature($consId . $secretKey . $timeStamp, $result['response']);
    $jsonData = decompress($decrypt);
    $objects = json_decode($jsonData, true); // decode JSON as associative array

    // Format tampilan dengan setiap objek pada baris terpisah
    $formattedData = '';
    foreach ($objects as $object) {
        $formattedData .= json_encode($object, JSON_PRETTY_PRINT) . "\n";
    }
} catch (\Throwable $th) {
    echo $th->getMessage();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Data JSON</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }
        th, td {
            padding: 12px 15px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        th {
            background-color: #f2f2f2;
            font-weight: bold;
        }
        td {
            background-color: #fff;
        }
        tr:hover {
            background-color: #f5f5f5;
        }
        pre {
            background-color: #f4f4f4;
            padding: 10px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
            white-space: pre-wrap;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
        }
        .btn:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <h1>Data JSON</h1>
    <a href="http://10.50.151.155/rsudciracas/bridgingbpjs/get-antrean-rs-antrian-per-hari.php" class="btn">Kembali ke Halaman Utama</a>
    <table>
        <thead>
            <tr>
                <th>Task ID</th>
                <th>Task Name</th>
                <th>Waktu</th>
                <th>Waktu RS</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($objects as $object): ?>
                <tr>
                    <td><?php echo htmlspecialchars($object['taskid']); ?></td>
                    <td><?php echo htmlspecialchars($object['taskname']); ?></td>
                    <td><?php echo htmlspecialchars($object['waktu']); ?></td>
                    <td><?php echo htmlspecialchars($object['wakturs']); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>
